       IDENTIFICATION DIVISION. 
       PROGRAM-ID. FINAL-TEST.
       AUTHOR. YANNAPON.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT INPUT-FILE ASSIGN TO "trader8.dat"
           ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD  INPUT-FILE.
       01  INPUT-BUFFER.
           88 END-OF-INPUT-FILE       VALUE HIGH-VALUE.
           05 PROVINCE                PIC X(2).
           05 MEMBER                  PIC X(4).
           05 INCOME                  PIC 9(6).
       
       WORKING-STORAGE SECTION. 
       01  P-ID                       PIC X(2).
       01  P-TOTAL                    PIC 9(9).
       01  MEM-ID                     PIC X(4).
       01  MEM-TOTAL                  PIC 9(6).
       01  MEM-ID-MAX                     PIC X(4).
       01  MEM-TOTAL-MAX                  PIC 9(6).
       01  HIGEST-P-ID                PIC X(2).
       01  HIGEST-P-INCOME            PIC 9(9) VALUE ZERO.

       01  PRT-HIGEST                 PIC $$$$,$$$,$$$.

       01  PRT-HEADER.
           05 FILLER                  PIC X(11) VALUE "PROVINCE".
           05 FILLER                  PIC X(3) VALUE  SPACES .
           05 FILLER                  PIC X(13) VALUE " P INCOME".
           05 FILLER                  PIC X(4) VALUE  SPACES .
           05 FILLER                  PIC X(13) VALUE "MEMBER".
           05 FILLER                  PIC X(2) VALUE  SPACES .
           05 FILLER                  PIC X(13) VALUE "MEMBER INCOME".

       01  PRT-ROW.
           05 PRT-ID                  PIC X(2).
           05 FILLER                  PIC X(3) VALUE  SPACES .
           05 PRT-P-INCOME            PIC $$$$,$$$,$$$.
           05 FILLER                  PIC X(4) VALUE  SPACES .
           05 PRT-MEMBER              PIC X(4).
           05 FILLER                  PIC X(2) VALUE  SPACES .
           05 PRT-MEM-INCOME          PIC $$$$,$$$.

       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT INPUT-FILE 
           DISPLAY PRT-HEADER 
           PERFORM READ-LINE
           PERFORM PROCESS-PROVINCE UNTIL END-OF-INPUT-FILE 
           CLOSE INPUT-FILE 
           MOVE HIGEST-P-INCOME TO PRT-HIGEST
           DISPLAY "MAX PROVINCE: ", HIGEST-P-ID WITH NO ADVANCING 
           DISPLAY "  SUM INCOME:  ",PRT-HIGEST
           GOBACK
           .
           
       PROCESS-PROVINCE.
           MOVE PROVINCE TO P-ID 
           MOVE P-ID TO PRT-ID
           MOVE ZEROES TO P-TOTAL 
           MOVE ZEROES TO MEM-TOTAL-MAX
           PERFORM PROCESS-INCOME UNTIL PROVINCE NOT = P-ID 
           DISPLAY PRT-ROW
           IF P-TOTAL > HIGEST-P-INCOME 
              MOVE P-TOTAL TO HIGEST-P-INCOME 
              MOVE P-ID TO HIGEST-P-ID 
           END-IF 
           .
          

       PROCESS-INCOME.
           MOVE MEMBER TO MEM-ID 
           MOVE INCOME  TO MEM-TOTAL 
           IF MEM-TOTAL > MEM-TOTAL-MAX  
              MOVE MEM-TOTAL TO MEM-TOTAL-MAX 
              MOVE MEM-ID TO MEM-ID-MAX 
              MOVE MEM-ID TO PRT-MEMBER
              MOVE MEM-TOTAL TO PRT-MEM-INCOME 
           END-IF 
           PERFORM PROCESS-LINE UNTIL PROVINCE NOT = P-ID 
              OR END-OF-INPUT-FILE
           MOVE P-TOTAL TO PRT-P-INCOME
           .
           
      
       PROCESS-LINE.
           ADD MEM-TOTAL TO P-TOTAL
           PERFORM READ-LINE
           .

       READ-LINE.
           READ INPUT-FILE 
              AT END SET END-OF-INPUT-FILE TO TRUE
           END-READ
           .
